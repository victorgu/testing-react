import React from 'react';
import Todos from './containers/Todos'

function App() {
  return (
    <div className="App">
      <Todos />
    </div>
  );
}

export default App;
