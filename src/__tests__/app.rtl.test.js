import React from 'react';
import { cleanup, render, fireEvent } from 'react-testing-library';
// this adds custom jest matchers from jest-dom
import 'jest-dom/extend-expect'

import App from '../App';

const wait = (wrapper, predicate, timeout = 10) => {
  return new Promise((resolve, reject) => {
    if (predicate(wrapper)) {
      return resolve(true);
    }
    setTimeout(() => {
      wrapper.update();
      return predicate(wrapper) ? resolve(true) : reject(new Error('Timeout expired'));
    }, timeout);
  });
};

// automatically unmount and cleanup DOM after the test is finished.
afterEach(cleanup)

test('entering a todo in form adds a todo', async () => {
  const { getByText, getByPlaceholderText, getByTestId, container } = render(<App />);

  // enter todo text in textbox
  getByPlaceholderText('Enter todo text').value = 'My new todo';

  // click Add
  fireEvent.click(getByText('Add'));

  // wait for Todo to show up
  // TODO: await wait(() => getByText('My new todo'));

  // make sure form is cleared
  expect(getByTestId('TodoForm-input').value).toEqual('');

  // make sure todo was added
  // TODO: expect(getByText('My new todo')).toBeDefined();
});
