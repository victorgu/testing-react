import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '../App';

configure({ adapter: new Adapter() });

let wrapper;

const wait = (wrapper, predicate, timeout = 10) => {
  return new Promise((resolve, reject) => {
    if (predicate(wrapper)) {
      return resolve(true);
    }
    setTimeout(() => {
      wrapper.update();
      return predicate(wrapper) ? resolve(true) : reject(new Error('Timeout expired'));
    }, timeout);
  });
};

test('entering a todo in form adds a todo', async () => {
  wrapper = mount(<App />);

  // enter todo text in textbox
  wrapper.find('.TodoForm-input').instance().value = 'My new todo';

  // click Add
  wrapper.find('.TodoForm-button').simulate('click');

  // wait for Todo to show up
  await wait(wrapper, w =>
    w
      .find('.TodoItem')
      .at(3)
      .exists()
  );

  // make sure form is cleared
  expect(wrapper.find('.TodoForm-input').instance().value).toEqual('');

  // make sure todo was added
  const newText = wrapper
    .find('.TodoItem')
    .at(3)
    .find('.TodoItem-text')
    .text();
  expect(newText).toEqual('My new todo');
});